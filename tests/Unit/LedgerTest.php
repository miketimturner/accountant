<?php

declare(strict_types=1);

namespace Altek\Accountant\Tests\Unit;

use Altek\Accountant\Ciphers\Base64;
use Altek\Accountant\Ciphers\Bleach;
use Altek\Accountant\Context;
use Altek\Accountant\Contracts\Recordable;
use Altek\Accountant\Exceptions\AccountantException;
use Altek\Accountant\Exceptions\DecipherException;
use Altek\Accountant\Notary;
use Altek\Accountant\Tests\AccountantTestCase;
use Altek\Accountant\Tests\Database\Factories\ArticleFactory;
use Altek\Accountant\Tests\Database\Factories\LedgerFactory;
use Altek\Accountant\Tests\Database\Factories\UserFactory;
use Altek\Accountant\Tests\Models\Article;
use Altek\Accountant\Tests\Models\User;
use Carbon\Carbon;
use DateTime;
use DateTimeInterface;
use Illuminate\Support\Facades\Config;

class LedgerTest extends AccountantTestCase
{
    /**
     * @group Ledger::compile
     * @test
     */
    public function itSuccessfullyCompilesLedgerData(): void
    {
        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ]);

        $ledger = $article->ledgers()->first();

        self::assertCount(18, $compiled = $ledger->compile());

        self::assertSame([
            'ledger_id'               => 1,
            'ledger_context'          => Context::TEST,
            'ledger_event'            => 'created',
            'ledger_url'              => 'Command Line Interface',
            'ledger_ip_address'       => '127.0.0.1',
            'ledger_user_agent'       => 'Symfony',
            'ledger_created_at'       => '2012-06-14 15:03:03',
            'ledger_updated_at'       => '2012-06-14 15:03:03',
            'ledger_signature'        => '6ed5592c2d8907d6ad934779b7aebfaed9d416a2fa014a22d671b689ad1dec33285749f98a1d7217539f654e7e213f0d250d5513d27ab0a8949f9a713432a483',
            'user_id'                 => null,
            'user_type'               => null,
            'recordable_title'        => 'Keeping Track Of Eloquent Model Changes',
            'recordable_content'      => 'First step: install the Accountant package.',
            'recordable_published_at' => '2012-06-14 15:03:03',
            'recordable_reviewed'     => 1,
            'recordable_updated_at'   => '2012-06-14 15:03:03',
            'recordable_created_at'   => '2012-06-14 15:03:03',
            'recordable_id'           => 1,
        ], $compiled);

        self::assertArrayHasKey('ledger_signature', $compiled);
    }

    /**
     * @group Ledger::compile
     * @test
     */
    public function itSuccessfullyCompilesLedgerDataIncludingUserAttributes(): void
    {
        $user = UserFactory::new()->create([
            'is_admin'   => 1,
            'first_name' => 'rick',
            'last_name'  => 'Sanchez',
            'email'      => 'rick@wubba-lubba-dub.dub',
        ]);

        $this->actingAs($user);

        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ]);

        $ledger = $article->ledgers()->first();

        self::assertCount(24, $compiled = $ledger->compile());

        self::assertSame([
            'ledger_id'               => 2,
            'ledger_context'          => Context::TEST,
            'ledger_event'            => 'created',
            'ledger_url'              => 'Command Line Interface',
            'ledger_ip_address'       => '127.0.0.1',
            'ledger_user_agent'       => 'Symfony',
            'ledger_created_at'       => '2012-06-14 15:03:03',
            'ledger_updated_at'       => '2012-06-14 15:03:03',
            'ledger_signature'        => '51514d4cdcc9a3653eae86c87d878e007b32c3f5e2d5c6f77b9375fccd39098c52cf0d40309da71f0621b1e50ae9f3a8505f0b17e1bb0814b496305efdb6ea5f',
            'user_id'                 => '1',
            'user_type'               => User::class,
            'user_is_admin'           => '1',
            'user_first_name'         => 'rick',
            'user_last_name'          => 'Sanchez',
            'user_email'              => 'rick@wubba-lubba-dub.dub',
            'user_created_at'         => '2012-06-14 15:03:03',
            'user_updated_at'         => '2012-06-14 15:03:03',
            'recordable_title'        => 'Keeping Track Of Eloquent Model Changes',
            'recordable_content'      => 'First step: install the Accountant package.',
            'recordable_published_at' => '2012-06-14 15:03:03',
            'recordable_reviewed'     => 1,
            'recordable_updated_at'   => '2012-06-14 15:03:03',
            'recordable_created_at'   => '2012-06-14 15:03:03',
            'recordable_id'           => 1,
        ], $compiled);

        self::assertArrayHasKey('ledger_signature', $compiled);
    }

    /**
     * @group Ledger::compile
     * @group Ledger::getProperty
     * @test
     */
    public function itReturnsTheAppropriateRecordableDataValues(): void
    {
        $user = UserFactory::new()->create([
            'is_admin'   => 1,
            'first_name' => 'rick',
            'last_name'  => 'Sanchez',
            'email'      => 'rick@wubba-lubba-dub.dub',
        ]);

        $this->actingAs($user);

        $ledger = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ])
            ->ledgers()
            ->first();

        // Compile data, making it available to the getProperty() method
        self::assertCount(24, $ledger->compile());

        // Mutate value
        self::assertSame('KEEPING TRACK OF ELOQUENT MODEL CHANGES', $ledger->getProperty('recordable_title'));
        self::assertSame('Rick', $ledger->getProperty('user_first_name'));

        // Cast value
        self::assertTrue($ledger->getProperty('user_is_admin'));
        self::assertTrue($ledger->getProperty('recordable_reviewed'));

        // Date value
        self::assertInstanceOf(DateTimeInterface::class, $ledger->getProperty('user_created_at'));
        self::assertInstanceOf(DateTimeInterface::class, $ledger->getProperty('recordable_published_at'));

        // Original value
        self::assertSame('First step: install the Accountant package.', $ledger->getProperty('recordable_content'));
        self::assertSame('Sanchez', $ledger->getProperty('user_last_name'));

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid property: "invalid_property"');

        // Fetch invalid property
        $ledger->getProperty('invalid_property');
    }

    /**
     * @group Ledger::getMetadata
     * @test
     */
    public function itReturnsTheLedgerMetadata(): void
    {
        $ledger = ArticleFactory::new()->create()->ledgers()->first();

        self::assertCount(11, $metadata = $ledger->getMetadata());

        self::assertArrayHasKey('ledger_signature', $metadata);

        unset($metadata['ledger_signature']);

        self::assertSame([
            'ledger_id'         => 1,
            'ledger_context'    => Context::TEST,
            'ledger_event'      => 'created',
            'ledger_url'        => 'Command Line Interface',
            'ledger_ip_address' => '127.0.0.1',
            'ledger_user_agent' => 'Symfony',
            'ledger_created_at' => '2012-06-14 15:03:03',
            'ledger_updated_at' => '2012-06-14 15:03:03',
            'user_id'           => null,
            'user_type'         => null,
        ], $metadata);
    }

    /**
     * @group Ledger::getMetadata
     * @test
     */
    public function itReturnsTheLedgerMetadataIncludingExtraUserAttributes(): void
    {
        $user = UserFactory::new()->create([
            'is_admin'   => 1,
            'first_name' => 'rick',
            'last_name'  => 'Sanchez',
            'email'      => 'rick@wubba-lubba-dub.dub',
        ]);

        $this->actingAs($user);

        $ledger = ArticleFactory::new()->create()->ledgers()->first();

        self::assertCount(17, $metadata = $ledger->getMetadata());

        self::assertArrayHasKey('ledger_signature', $metadata);

        unset($metadata['ledger_signature']);

        self::assertSame([
            'ledger_id'         => 2,
            'ledger_context'    => Context::TEST,
            'ledger_event'      => 'created',
            'ledger_url'        => 'Command Line Interface',
            'ledger_ip_address' => '127.0.0.1',
            'ledger_user_agent' => 'Symfony',
            'ledger_created_at' => '2012-06-14 15:03:03',
            'ledger_updated_at' => '2012-06-14 15:03:03',
            'user_id'           => 1,
            'user_type'         => User::class,
            'user_is_admin'     => true,
            'user_first_name'   => 'Rick',
            'user_last_name'    => 'Sanchez',
            'user_email'        => 'rick@wubba-lubba-dub.dub',
            'user_created_at'   => '2012-06-14 15:03:03',
            'user_updated_at'   => '2012-06-14 15:03:03',
        ], $metadata);
    }

    /**
     * @group Ledger::getMetadata
     * @test
     *
     * @dataProvider dateFormatProvider
     *
     * @param string      $expected
     * @param string|null $dateFormat
     */
    public function itSuccessfullyVerifiesTheDateFormat(string $expected, string $dateFormat = null): void
    {
        Config::set('accountant.ledger.date_format', $dateFormat);

        $user = UserFactory::new()->create();

        $this->actingAs($user);

        $ledger = ArticleFactory::new()->create()->ledgers()->first();

        $metadata = $ledger->getMetadata();

        self::assertSame($expected, $metadata['ledger_created_at']);
        self::assertSame($expected, $metadata['ledger_updated_at']);
        self::assertSame($expected, $metadata['user_created_at']);
        self::assertSame($expected, $metadata['user_updated_at']);
    }

    /**
     * @return array
     */
    public function dateFormatProvider(): array
    {
        return [
            [
                '2012-06-14T15:03:03.000000Z',
                null,
            ],
            [
                '2012-06-14 15:03:03',
                'Y-m-d H:i:s',
            ],
            [
                '2012-06-14T15:03:03+00:00',
                DateTime::ATOM,
            ],
            [
                '2012-06-14T15:03:03+00:00',
                DateTime::RFC3339,
            ],
            [
                '2012-06-14T15:03:03.000+00:00',
                DateTime::RFC3339_EXTENDED,
            ],
        ];
    }

    /**
     * @group Ledger::getData
     * @test
     */
    public function itOnlyReturnsTheModifiedRecordableData(): void
    {
        $article = ArticleFactory::new()->create([
            'title'        => 'Keeping Track Of Eloquent Model Changes',
            'content'      => 'First step: install the Accountant package.',
            'reviewed'     => 1,
            'published_at' => Carbon::now(),
        ]);

        $ledger = $article->ledgers()->first();

        self::assertCount(7, $data = $ledger->getData());

        self::assertSame([
            'title'        => 'KEEPING TRACK OF ELOQUENT MODEL CHANGES',
            'content'      => 'First step: install the Accountant package.',
            'published_at' => '2012-06-14T15:03:03.000000Z',
            'reviewed'     => true,
            'updated_at'   => '2012-06-14T15:03:03.000000Z',
            'created_at'   => '2012-06-14T15:03:03.000000Z',
            'id'           => 1,
        ], $data);
    }

    /**
     * @group Ledger::getData
     * @test
     */
    public function itReturnsAllTheRecordableData(): void
    {
        $ledger = LedgerFactory::new()->create([
            'event'           => 'updated',
            'recordable_type' => Article::class,
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        self::assertCount(1, $ledger->getData());
        self::assertCount(7, $ledger->getData(true));
    }

    /**
     * @group Ledger::getData
     * @test
     */
    public function itReturnsDecipheredRecordableData(): void
    {
        $article = new class() extends Article {
            protected function serializeDate(DateTimeInterface $date): string
            {
                return $date->format(Carbon::RFC850);
            }

            protected $ciphers = [
                'title'   => Base64::class,
                'content' => Bleach::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'event'           => 'updated',
            'recordable_type' => \get_class($article),
            'properties'      => [
                'title'        => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
                'content'      => '--------------------------------------kage.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        self::assertCount(1, $modified = $ledger->getData());
        self::assertCount(7, $all = $ledger->getData(true));

        self::assertSame([
            'content' => '--------------------------------------kage.',
        ], $modified);

        self::assertSame([
            'title'        => 'KEEPING TRACK OF ELOQUENT MODEL CHANGES',
            'content'      => '--------------------------------------kage.',
            'published_at' => 'Monday, 18-Jun-12 21:32:34 UTC',
            'reviewed'     => true,
            'updated_at'   => 'Saturday, 24-Oct-15 23:11:10 UTC',
            'created_at'   => 'Thursday, 14-Jun-12 15:03:03 UTC',
            'id'           => 1,
        ], $all);
    }

    /**
     * @group Ledger::getData
     * @test
     */
    public function itReturnsDecipheredRecordableDataFromDeletedRecord(): void
    {
        $article = new class() extends Article {
            protected function serializeDate(DateTimeInterface $date): string
            {
                return $date->format(Carbon::RFC822);
            }

            protected $ciphers = [
                'title' => Base64::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'event'           => 'forceDeleted',
            'recordable_type' => \get_class($article),
            'recordable_id'   => 1,
            'properties'      => [
                'title'        => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        self::assertCount(1, $modified = $ledger->getData());
        self::assertCount(7, $all = $ledger->getData(true));

        self::assertSame([
            'content' => 'First step: install the Accountant package.',
        ], $modified);

        self::assertSame([
            'title'        => 'KEEPING TRACK OF ELOQUENT MODEL CHANGES',
            'content'      => 'First step: install the Accountant package.',
            'published_at' => 'Mon, 18 Jun 12 21:32:34 +0000',
            'reviewed'     => true,
            'updated_at'   => 'Sat, 24 Oct 15 23:11:10 +0000',
            'created_at'   => 'Thu, 14 Jun 12 15:03:03 +0000',
            'id'           => 1,
        ], $all);
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itFailsToCompileLedgerDataDueToInvalidProperty(): void
    {
        $article               = new class() extends Article {
            protected $ciphers = [
                'invalid_property' => Base64::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'recordable_type' => \get_class($article),
        ]);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid property: "invalid_property"');

        $ledger->extract();
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itFailsToCompileLedgerDataDueToInvalidCipherImplementation(): void
    {
        $article               = new class() extends Article {
            protected $ciphers = [
                'title' => AccountantTestCase::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'recordable_type' => \get_class($article),
            'properties'      => [
                'title' => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
            ],
        ]);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid Cipher implementation: "Altek\Accountant\Tests\AccountantTestCase"');

        $ledger->extract();
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itFailsToExtractRecordableInstanceDueToTaintedLedgerDateMismatch(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Extraction failed due to tainted data');

        $ledger = LedgerFactory::new()->create([
            'created_at' => '1975-07-25 16:25:34',
            'updated_at' => '1981-12-16 03:33:01',
        ]);

        $ledger->extract();
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itFailsToExtractRecordableInstanceDueToTaintedLedgerSignatureMismatch(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Extraction failed due to tainted data');

        $ledger = LedgerFactory::new()->create([
            'signature' => '',
        ]);

        $ledger->extract();
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itFailsToExtractRecordableInstanceFromLedgerInStrictMode(): void
    {
        $article               = new class() extends Article {
            protected $ciphers = [
                'title'   => Base64::class,
                'content' => Bleach::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'event'           => 'updated',
            'recordable_type' => \get_class($article),
            'properties'      => [
                'title'        => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
                'content'      => '--------------------------------------kage.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        try {
            $ledger->extract();
        } catch (DecipherException $exception) {
            self::assertSame('Value deciphering is not supported by this implementation', $exception->getMessage());
            self::assertSame('--------------------------------------kage.', $exception->getCipheredValue());
        }
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itSuccessfullyExtractsRecordableInstanceFromLedger(): void
    {
        $article               = new class() extends Article {
            protected $ciphers = [
                'title'   => Base64::class,
                'content' => Bleach::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'event'           => 'updated',
            'recordable_type' => \get_class($article),
            'properties'      => [
                'title'        => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
                'content'      => '--------------------------------------kage.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        $article = $ledger->extract(false);

        self::assertInstanceOf(Recordable::class, $article);
        self::assertInstanceOf(Article::class, $article);

        // The relation to the existing model should exist
        self::assertNotNull($ledger->recordable);
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itSuccessfullyExtractsDeletedRecordableInstanceFromLedger(): void
    {
        $article               = new class() extends Article {
            protected $ciphers = [
                'title' => Base64::class,
            ];
        };

        $ledger = LedgerFactory::new()->create([
            'event'           => 'forceDeleted',
            'recordable_type' => \get_class($article),
            'recordable_id'   => 1,
            'properties'      => [
                'title'        => 'S2VlcGluZyBUcmFjayBPZiBFbG9xdWVudCBNb2RlbCBDaGFuZ2Vz',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        $article = $ledger->extract();

        self::assertInstanceOf(Recordable::class, $article);
        self::assertInstanceOf(Article::class, $article);

        // The relation to the deleted model shouldn't exist
        self::assertNull($ledger->recordable);

        // Make sure the title mutator works
        self::assertSame('KEEPING TRACK OF ELOQUENT MODEL CHANGES', $article->title);
    }

    /**
     * @group Ledger::extract
     * @test
     */
    public function itSuccessfullyExtractsRecordableInstanceFromLedgerInStrictMode(): void
    {
        $ledger = LedgerFactory::new()->create([
            'event'           => 'updated',
            'recordable_type' => Article::class,
            'properties'      => [
                'title'        => 'Keeping Track Of Eloquent Model Changes',
                'content'      => 'First step: install the Accountant package.',
                'published_at' => '2012-06-18 21:32:34',
                'reviewed'     => true,
                'updated_at'   => '2015-10-24 23:11:10',
                'created_at'   => '2012-06-14 15:03:03',
                'id'           => 1,
            ],
            'modified' => [
                'content',
            ],
        ]);

        $article = $ledger->extract();

        self::assertInstanceOf(Recordable::class, $article);
        self::assertInstanceOf(Article::class, $article);
    }

    /**
     * @group Ledger::isTainted
     * @test
     */
    public function itFailsToAssertTheRecordIsTaintedDueToMissingTimestamps(): void
    {
        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('The use of timestamps is required');

        $article = LedgerFactory::new()->make();

        $article->timestamps = false;

        $article->isTainted();
    }

    /**
     * @group Ledger::isTainted
     * @test
     */
    public function itFailsToAssertTheRecordIsTaintedDueToInvalidNotaryImplementation(): void
    {
        $ledger = LedgerFactory::new()->create();

        $this->app['config']->set('accountant.notary', self::class);

        $this->expectException(AccountantException::class);
        $this->expectExceptionMessage('Invalid Notary implementation: "Altek\Accountant\Tests\Unit\LedgerTest"');

        $ledger->isTainted();
    }

    /**
     * @group Ledger::isTainted
     * @test
     */
    public function itSuccessfullyAssertsTheRecordIsTaintedDueToMismatchingDates(): void
    {
        $ledger = LedgerFactory::new()->create([
            'updated_at' => '2015-10-24 23:11:10',
            'created_at' => '2012-06-14 15:03:03',
        ]);

        self::assertTrue($ledger->isTainted());
    }

    /**
     * @group Ledger::isTainted
     * @test
     */
    public function itSuccessfullyAssertsTheRecordIsTaintedDueToMismatchingSignatures(): void
    {
        $ledger = LedgerFactory::new()->create([
            'signature' => Notary::sign([
                'completely' => 'different',
                'array'      => 'structure',
                'from'       => 'the',
                'one'        => 'used',
                'when'       => 'signing',
            ]),
        ]);

        self::assertTrue($ledger->isTainted());
    }

    /**
     * @group Ledger::isTainted
     * @test
     */
    public function itSuccessfullyAssertsTheRecordIsNotTainted(): void
    {
        $ledger = LedgerFactory::new()->create();

        self::assertFalse($ledger->isTainted());
    }
}
